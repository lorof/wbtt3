import React from 'react';
import styles from 'app.module.scss';
import { Arrows } from 'arrows';

export const App = () => {
	return (
		<div className={styles.app}>
			<Arrows />
		</div>
	);
};
