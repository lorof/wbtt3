import { Arrow } from 'arrows/arrows.types';

export const arrows = [
	{ id: 'TdI4lkuvBK19AWquhjOYb', direction: 'ArrowLeft' },
	{ id: '6N2swPjyaa8PgoYiwInK9', direction: 'ArrowRight' },
	{ id: 'BVRGse5V9d50kRNIGRUJw', direction: 'ArrowDown' },
] as Arrow[];

export const wonPressedArrows = {
	TdI4lkuvBK19AWquhjOYb: true,
	'6N2swPjyaa8PgoYiwInK9': true,
	BVRGse5V9d50kRNIGRUJw: true,
};

export const lostPressedArrows = {
	TdI4lkuvBK19AWquhjOYb: false,
	'6N2swPjyaa8PgoYiwInK9': false,
	BVRGse5V9d50kRNIGRUJw: false,
};
