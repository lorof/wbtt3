import { runSaga, Saga } from 'redux-saga';
import { getArrowSaga, pressArrowSaga, resetArrowsSaga } from 'arrows/arrows.saga';
import { arrowsApi } from 'arrows/arrows.api';
import { arrows } from 'arrows/__fixtures__/arrows';
import { arrowsActions, InitialState } from 'arrows/arrows.slice';

const state = {
	arrowsReducer: {
		arrows: [],
		pressedArrows: {},
	} as InitialState,
};

type Action<T> = { type: string; payload: T };

describe('The arrows saga', () => {
	it('get arrow saga', async () => {
		const requestArrow = jest
			.spyOn(arrowsApi, 'getRandomArrow')
			.mockImplementation(() => Promise.resolve(arrows[0]));

		const dispatched: Action<InitialState>[] = [];

		await runSaga(
			{
				dispatch: (action: Action<InitialState>) => dispatched.push(action),
				getState: () => state,
			},
			getArrowSaga as Saga
		);

		expect(requestArrow).toHaveBeenCalledTimes(1);
		expect(dispatched).toEqual([
			arrowsActions.add({ arrows: [arrows[0]], pressedArrows: {} }),
		]);
		requestArrow.mockClear();
	});

	it('press arrow saga', async () => {
		const dispatched: Action<{}>[] = [];

		await runSaga(
			{
				dispatch: (action: Action<{}>) => dispatched.push(action),
				getState: () => state,
			},
			pressArrowSaga as Saga
		);

		expect(dispatched).toEqual([arrowsActions.press({})]);
	});

	it('reset saga', async () => {
		const dispatched: Action<undefined>[] = [];

		await runSaga(
			{
				dispatch: (action: Action<undefined>) => dispatched.push(action),
				getState: () => state,
			},
			resetArrowsSaga as Saga
		);

		expect(dispatched).toEqual([arrowsActions.reset()]);
	});
});
