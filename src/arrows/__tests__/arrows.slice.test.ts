import { arrowsReducer, arrowsActions } from 'arrows/arrows.slice';
import { lostPressedArrows, arrows } from 'arrows/__fixtures__/arrows';

const addPayload = { arrows, pressedArrows: lostPressedArrows };
const initialState = { arrows: [], pressedArrows: {} };

describe('The arrows slice', () => {
	it('add action', () => {
		expect(arrowsReducer(initialState, arrowsActions.add(addPayload))).toEqual(
			addPayload
		);
	});

	it('press action', () => {
		expect(arrowsReducer(addPayload, arrowsActions.press(lostPressedArrows))).toEqual(
			addPayload
		);
	});

	it('reset action', () => {
		expect(arrowsReducer(addPayload, arrowsActions.reset())).toEqual(initialState);
	});
});
