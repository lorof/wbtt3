import React from 'react';
import { Arrows, GAME_ALERTS } from 'arrows/arrows';
import { renderWithProviders } from 'common/test-utils';
import { lostPressedArrows, wonPressedArrows, arrows } from 'arrows/__fixtures__/arrows';

beforeEach(() => {
	Object.defineProperty(window, 'alert', {
		value: jest.fn(),
	});
});

describe('The <Arrows> component', () => {
	it('should render 3 arrows', () => {
		const { getAllByText } = renderWithProviders(<Arrows />, {
			preloadedState: {
				arrowsReducer: {
					pressedArrows: lostPressedArrows,
					arrows,
				},
			},
		});

		expect(getAllByText(/←|→|↓/).length).toBe(3);
	});

	it('should call alert with lost title', () => {
		renderWithProviders(<Arrows />, {
			preloadedState: {
				arrowsReducer: {
					pressedArrows: lostPressedArrows,
					arrows,
				},
			},
		});

		expect(window.alert).toBeCalledWith(GAME_ALERTS.lost);
	});

	it('should call alert with won title', () => {
		renderWithProviders(<Arrows />, {
			preloadedState: {
				arrowsReducer: {
					pressedArrows: wonPressedArrows,
					arrows,
				},
			},
		});

		expect(window.alert).toBeCalledWith(GAME_ALERTS.won);
	});

	it('matches snapshot', () => {
		const { asFragment } = renderWithProviders(<Arrows />, {
			preloadedState: {
				arrowsReducer: {
					pressedArrows: lostPressedArrows,
					arrows,
				},
			},
		});

		expect(asFragment()).toMatchSnapshot();
	});
});
