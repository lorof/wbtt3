import { nanoid } from 'nanoid';

export enum Arrows {
	ArrowUp = 'ArrowUp',
	ArrowDown = 'ArrowDown',
	ArrowLeft = 'ArrowLeft',
	ArrowRight = 'ArrowRight',
}

const arrowArr = [Arrows.ArrowUp, Arrows.ArrowDown, Arrows.ArrowRight, Arrows.ArrowLeft];

export const arrowsApi = {
	getRandomArrow: async () => ({
		id: nanoid(),
		direction: arrowArr[Math.floor(Math.random() * arrowArr.length)],
	}),
};
