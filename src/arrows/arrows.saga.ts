import { Task } from 'redux-saga';
import {
	put,
	takeEvery,
	call,
	select,
	delay,
	cancel,
	cancelled,
	take,
	fork,
} from 'redux-saga/effects';
import { arrowsApi } from './arrows.api';
import { arrowsActions, arrowsSelector, pressedArrowsSelector } from './arrows.slice';
import { Arrow } from './arrows.types';

export enum ArrowActions {
	startPolling = 'startPolling',
	stopPolling = 'stopPolling',
	getArrow = 'getArrow',
	pressArrow = 'pressArrow',
	resetArrows = 'resetArrows',
}

const STACK_SIZE = 10;
const TIMEOUT = 3000;

export const getArrow = () => ({ type: ArrowActions.getArrow });
export const resetArrows = () => ({ type: ArrowActions.resetArrows });
export const pressArrow = (payload: Arrow['direction']) => ({
	type: ArrowActions.pressArrow,
	payload,
});
export const startPolling = () => ({ type: ArrowActions.startPolling });
export const stopPolling = () => ({ type: ArrowActions.stopPolling });

export function* getArrowSaga() {
	const arrows: Arrow[] = yield select(arrowsSelector);
	const pressedArrows: Record<string, boolean> = yield select(pressedArrowsSelector);

	const arrow: Arrow = yield call(arrowsApi.getRandomArrow);

	const [lastArrow] = arrows.slice(-1);

	const newArrows = [...arrows, arrow].slice(-STACK_SIZE);

	yield put(
		arrowsActions.add({
			arrows: newArrows,
			pressedArrows: {
				...pressedArrows,
				...(lastArrow && pressedArrows[lastArrow.id] === undefined
					? { [lastArrow.id]: false }
					: {}),
			},
		})
	);
}
export function* pressArrowSaga(action: ReturnType<typeof pressArrow>) {
	const arrows: Arrow[] = yield select(arrowsSelector);
	const pressedArrows: Record<string, boolean> = yield select(pressedArrowsSelector);

	const [lastArrow] = arrows.slice(-1);

	yield put(
		arrowsActions.press({
			...pressedArrows,
			...(lastArrow && pressedArrows[lastArrow.id] === undefined
				? {
						[lastArrow.id]: lastArrow.direction === action.payload,
				  }
				: {}),
		})
	);
}
export function* resetArrowsSaga() {
	yield put(arrowsActions.reset());
}

export function* startPollingSaga() {
	const task: Task = yield fork(function*() {
		const isCancelled: boolean = yield cancelled();

		while (!isCancelled) {
			yield put(getArrow());
			yield delay(TIMEOUT);
		}
	});

	yield take(ArrowActions.stopPolling);

	yield cancel(task);
}

export default function* arrowsRootSaga() {
	yield takeEvery(ArrowActions.getArrow, getArrowSaga);
	yield takeEvery(ArrowActions.pressArrow, pressArrowSaga);
	yield takeEvery(ArrowActions.resetArrows, resetArrowsSaga);
	yield takeEvery(ArrowActions.startPolling, startPollingSaga);
}
