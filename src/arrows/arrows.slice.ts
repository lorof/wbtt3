import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Arrow } from 'arrows/arrows.types';
import { RootState } from 'store';

export type InitialState = {
	arrows: Arrow[];
	pressedArrows: Record<string, boolean>;
};

const initialState: InitialState = {
	arrows: [],
	pressedArrows: {},
};

export const arrowsSlice = createSlice({
	name: 'arrows',

	initialState,

	reducers: {
		add: (state, { payload }: PayloadAction<InitialState>) => {
			state.arrows = payload.arrows;
			state.pressedArrows = payload.pressedArrows;
		},

		press: (state, { payload }: PayloadAction<InitialState['pressedArrows']>) => {
			state.pressedArrows = payload;
		},

		reset: (state) => {
			state.arrows = initialState.arrows;
			state.pressedArrows = initialState.pressedArrows;
		},
	},
});

export const { reducer: arrowsReducer, actions: arrowsActions } = arrowsSlice;

export const arrowsSelector = (store: RootState) => store.arrowsReducer.arrows;
export const pressedArrowsSelector = (store: RootState) =>
	store.arrowsReducer.pressedArrows;
