import React, { useEffect, useMemo } from 'react';
import classNames from 'classnames';
import { Arrows as ArrowTypes } from 'arrows/arrows.api';
import { useHotKey } from 'common/hooks/use-hotkey';
import { useAppDispatch, useAppSelector } from 'store';
import { pressArrow, resetArrows, startPolling, stopPolling } from './arrows.saga';
import styles from 'arrows/arrows.module.scss';
import { arrowsSelector, pressedArrowsSelector } from './arrows.slice';

const ARROWS_TITLE = {
	[ArrowTypes.ArrowUp]: '↑',
	[ArrowTypes.ArrowDown]: '↓',
	[ArrowTypes.ArrowRight]: '→',
	[ArrowTypes.ArrowLeft]: '←',
};

const LIVES = 3;
const ARROWS_COUNT = 3;

export const GAME_ALERTS = {
	won: 'you won!',
	lost: 'you lost!',
};

export const Arrows = () => {
	const arrows = useAppSelector(arrowsSelector);
	const pressedArrows = useAppSelector(pressedArrowsSelector);
	const appDispatch = useAppDispatch();

	const handleKeyPress = (direction: ArrowTypes) => () => {
		appDispatch(pressArrow(direction));
	};

	const correctArrows = useMemo(() => {
		return arrows.filter(
			(arrow) =>
				pressedArrows[arrow.id] !== undefined && pressedArrows[arrow.id] === true
		);
	}, [arrows, pressedArrows]);

	const correctArrowsReachLimit = correctArrows.length >= ARROWS_COUNT;

	const incorrectArrows = useMemo(() => {
		return arrows.filter(
			(arrow) =>
				pressedArrows[arrow.id] !== undefined && pressedArrows[arrow.id] === false
		);
	}, [arrows, pressedArrows]);

	const incorrectArrowsReachLimit = incorrectArrows.length >= ARROWS_COUNT;

	useHotKey(ArrowTypes.ArrowUp, handleKeyPress(ArrowTypes.ArrowUp));
	useHotKey(ArrowTypes.ArrowDown, handleKeyPress(ArrowTypes.ArrowDown));
	useHotKey(ArrowTypes.ArrowLeft, handleKeyPress(ArrowTypes.ArrowLeft));
	useHotKey(ArrowTypes.ArrowRight, handleKeyPress(ArrowTypes.ArrowRight));

	useEffect(() => {
		const arrowsReachLimit = correctArrowsReachLimit || incorrectArrowsReachLimit;

		if (!arrowsReachLimit) return;

		appDispatch(stopPolling());
	}, [correctArrowsReachLimit, incorrectArrowsReachLimit]);

	useEffect(() => {
		if (!correctArrowsReachLimit) return;

		globalThis.alert(GAME_ALERTS.won);
	}, [correctArrowsReachLimit]);
	useEffect(() => {
		if (!incorrectArrowsReachLimit) return;

		globalThis.alert(GAME_ALERTS.lost);
	}, [incorrectArrowsReachLimit]);

	useEffect(() => {
		appDispatch(startPolling());

		return () => {
			appDispatch(stopPolling());
		};
	}, []);

	const handleRestart = () => {
		appDispatch(resetArrows());

		appDispatch(startPolling());
	};

	return (
		<>
			{!!arrows.length &&
				(correctArrowsReachLimit || incorrectArrowsReachLimit) && (
					<button onClick={handleRestart}>restart</button>
				)}
			{arrows.map((arrow) => (
				<div
					key={arrow.id}
					className={classNames({
						[styles.correctArrow]: pressedArrows[arrow.id] === true,
						[styles.incorrectArrow]: pressedArrows[arrow.id] === false,
					})}
				>
					{ARROWS_TITLE[arrow.direction]}
				</div>
			))}
			lives: {LIVES - incorrectArrows.length}
		</>
	);
};
