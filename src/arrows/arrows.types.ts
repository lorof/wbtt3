import { Arrows } from 'arrows/arrows.api';

export type Arrow = {
	id: string;
	direction: Arrows;
};
