import isHotkey from 'is-hotkey';
import { useEffect, useRef } from 'react';

export const useHotKey = <T extends Function>(key: string, cb: T) => {
	const cbRef = useRef(cb);

	cbRef.current = cb;

	useEffect(() => {
		const isKey = isHotkey(key);

		const handler = (event: KeyboardEvent) => {
			if (!isKey(event)) return;

			cbRef.current();
		};

		window.addEventListener('keydown', handler);

		return () => window.removeEventListener('keydown', handler);
	}, []);
};
