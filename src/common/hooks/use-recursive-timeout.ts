import { useEffect, useRef } from 'react';

const makeRecursiveTimeout = () => {
	let timeout: ReturnType<typeof setTimeout>;

	return function recursiveSetTimeout<T extends Function>(callback: T, time: number) {
		callback();

		timeout = setTimeout(() => {
			recursiveSetTimeout(callback, time);
		}, time);

		return () => {
			clearTimeout(timeout);
		};
	};
};

const recursiveSetTimeout = makeRecursiveTimeout();

export const useRecursiveTimeout = <T extends Function>(cb: T, time?: number | null) => {
	const cbRef = useRef(cb);

	cbRef.current = cb;

	useEffect(() => {
		let unmounted = false;

		if (!time) return;

		const cancel = recursiveSetTimeout(() => {
			if (!unmounted) {
				cbRef.current();
			}
		}, time);

		return () => {
			cancel();
			unmounted = true;
		};
	}, [time]);
};
