/// <reference types="react-scripts" />

declare module 'is-hotkey' {
	export default function isHotkey(
		hotkey: string | string[]
	): (event: KeyboardEvent) => boolean;
	export default function isHotkey(hotkey: string | string[], event: KeyboardEvent);
	export default function isHotkey(
		hotkey: string | string[],
		options: { byKey: boolean },
		event: KeyboardEvent
	);
}
